/** 
* @projectDescription    Wiseman Says
*
* @author   Roccoor Multimedia
* @version  1.0
*/

// Create namespace ///////////////////////////////////////////////////////////
if (!window.com) com = {};
if (!com.roccoor) com.roccoor = {};
if (!com.roccoor.about) com.roccoor.about = {};

// Functions //////////////////////////////////////////////////////////////////

/**
 * Opens a link
 * @alias openLink
 * @alias com.roccoor.about.openLink
 */