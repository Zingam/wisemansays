/** 
* @projectDescription    Wiseman Says
*
* @author   Roccoor Multimedia
* @version  1.0
*/

// Create namespace ///////////////////////////////////////////////////////////
if (!window.com) com = {};
if (!com.roccoor) com.roccoor = {};
if (!com.roccoor.bbm) com.roccoor.bbm = {};

// Functions //////////////////////////////////////////////////////////////////

var registered = false;
var uuid = { uuid: '4d4e1e00-7d35-11e2-9e96-0800200c9a66' };

/*
	4d4e1e00-7d35-11e2-9e96-0800200c9a66
	4d4e1e01-7d35-11e2-9e96-0800200c9a66
	4d4e1e02-7d35-11e2-9e96-0800200c9a66
	4d4e1e03-7d35-11e2-9e96-0800200c9a66
	4d4e1e04-7d35-11e2-9e96-0800200c9a66
*/

/**
 * Register the application to access the BBM Social Platform 
 * @alias register
 * @alias com.roccoor.bbm.register
 */
com.roccoor.bbm.register = function() {
	if (!registered)
		blackberry.event.addEventListener('onaccesschanged', com.roccoor.bbm.accessChangedCallback);
}

/**
 * Callback function to register the application with BBM used by com.roccoor.bbm.register
 * @alias accessChangedCallback
 * @alias com.roccoor.bbm.accessChangedCallback
 */
com.roccoor.bbm.accessChangedCallback = function(accessible, status) {
	if (status === 'allowed')
		// Access is allowed
		registered = accessible;
	else if (status === 'unregistered')
		// Register the application with BBM Social Platform
		blackberry.bbm.platform.register(uuid);
	else if (status === 'registered')
		alert('Access is pending and being processed.');
	else if (status === 'user')
		alert('Access is blocked by user');
	else if (status === 'rim')
		alert('Access is blocked by RIM');
	else if (status === 'nodata')
		alert('Access is blocked because the device is out of data coverage. A data connection is required to register application.');
	else if (status === 'unexpectederror')
		alert('Access is blocked because an unexpected error has occured.');
	else if (status === 'invaliduuid')
		alert('Access is blocked because an invalid UUID was provided when registering.');
	else if (status === 'temperror')
		alert('Access is blocked because of temporary error. Try again later.');
}

/** 
 * Invite BBM contacts to download the application
 * @alias inviteToDownload
 * @alias com.roccoor.bbm.inviteToDownload
 */
com.roccoor.bbm.inviteToDownload = function() {
	if(registered)
		blackberry.bbm.platform.users.inviteToDownload();
	else
		alert('Application is not yet registered. Please try again!');
}